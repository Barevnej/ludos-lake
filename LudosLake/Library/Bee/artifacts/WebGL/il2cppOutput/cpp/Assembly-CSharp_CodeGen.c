﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.InputSystem.InputActionAsset PlayerActions::get_asset()
extern void PlayerActions_get_asset_m29F00B42636E9B80C6DF87F9A9B83C9C881FCF36 (void);
// 0x00000002 System.Void PlayerActions::.ctor()
extern void PlayerActions__ctor_m71B1566835E2A2243BA0163398A8130FADFF3E63 (void);
// 0x00000003 System.Void PlayerActions::Dispose()
extern void PlayerActions_Dispose_mE7319569DEEED044CEF4B5193AF0B2F13633D733 (void);
// 0x00000004 System.Nullable`1<UnityEngine.InputSystem.InputBinding> PlayerActions::get_bindingMask()
extern void PlayerActions_get_bindingMask_m76272F21A20F33C2510D9D699CEE7A16DF3F6D78 (void);
// 0x00000005 System.Void PlayerActions::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void PlayerActions_set_bindingMask_m557941BC7F5C3BD7EC83E28A2EC7FCDF2CC10D98 (void);
// 0x00000006 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> PlayerActions::get_devices()
extern void PlayerActions_get_devices_m21C9DB4AC617CA00A5EDFE4F2067CFC3A383E9F2 (void);
// 0x00000007 System.Void PlayerActions::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void PlayerActions_set_devices_m46D23CC268E0ACCA7153C01E0B24AD2BD5033305 (void);
// 0x00000008 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> PlayerActions::get_controlSchemes()
extern void PlayerActions_get_controlSchemes_m100DBBF22D804B3F8A7E63B51A72ACF585D486A4 (void);
// 0x00000009 System.Boolean PlayerActions::Contains(UnityEngine.InputSystem.InputAction)
extern void PlayerActions_Contains_m1996C81A1DEC87153D96F1A7F5E95B0286543375 (void);
// 0x0000000A System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> PlayerActions::GetEnumerator()
extern void PlayerActions_GetEnumerator_m64E04259CC1A157904C654A95B8F0448A2F5D409 (void);
// 0x0000000B System.Collections.IEnumerator PlayerActions::System.Collections.IEnumerable.GetEnumerator()
extern void PlayerActions_System_Collections_IEnumerable_GetEnumerator_m147E7F0824387F22086B7E30AE31AC17379B6E77 (void);
// 0x0000000C System.Void PlayerActions::Enable()
extern void PlayerActions_Enable_mB26A7C4A622706BA8A679BD26EEB7A2B946881DA (void);
// 0x0000000D System.Void PlayerActions::Disable()
extern void PlayerActions_Disable_m069852B96E458E70A89447C30705DF2766E11D36 (void);
// 0x0000000E System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding> PlayerActions::get_bindings()
extern void PlayerActions_get_bindings_m74298D95C84A686A43A353FB95FC1D81D2C5F87A (void);
// 0x0000000F UnityEngine.InputSystem.InputAction PlayerActions::FindAction(System.String,System.Boolean)
extern void PlayerActions_FindAction_mE71FBCB5FF45543112F0F49E28A792D08E6EC8AA (void);
// 0x00000010 System.Int32 PlayerActions::FindBinding(UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputAction&)
extern void PlayerActions_FindBinding_m725102DA2718F391A9436716D41E7B14CEB0EDC4 (void);
// 0x00000011 PlayerActions/PlayerInputActions PlayerActions::get_PlayerInput()
extern void PlayerActions_get_PlayerInput_m496463FD573EA2247A563463628B6D1F6E47127F (void);
// 0x00000012 System.Void PlayerActions/PlayerInputActions::.ctor(PlayerActions)
extern void PlayerInputActions__ctor_mD110058A0B99BFDE4CDCD9AFE8027AE867EA0821 (void);
// 0x00000013 UnityEngine.InputSystem.InputAction PlayerActions/PlayerInputActions::get_SelectObject()
extern void PlayerInputActions_get_SelectObject_m86FF3A77E62AB507B68E37AEB25458C51BBE48B4 (void);
// 0x00000014 UnityEngine.InputSystem.InputAction PlayerActions/PlayerInputActions::get_Roll()
extern void PlayerInputActions_get_Roll_mE3BEACD52114FB801E4BB71A7967C4EB20F92F68 (void);
// 0x00000015 UnityEngine.InputSystem.InputActionMap PlayerActions/PlayerInputActions::Get()
extern void PlayerInputActions_Get_m2E2FD12DD34A0E939840DCE0C355B82A58EAD3D1 (void);
// 0x00000016 System.Void PlayerActions/PlayerInputActions::Enable()
extern void PlayerInputActions_Enable_m8EDBCF1082E3F8F4EF0D41AFBE7F25A0B4A44AEA (void);
// 0x00000017 System.Void PlayerActions/PlayerInputActions::Disable()
extern void PlayerInputActions_Disable_m4DB32972E104D79F5C70A876F62CC3D4E0182741 (void);
// 0x00000018 System.Boolean PlayerActions/PlayerInputActions::get_enabled()
extern void PlayerInputActions_get_enabled_mE88F22F42229857B001649C56D27581399997BDA (void);
// 0x00000019 UnityEngine.InputSystem.InputActionMap PlayerActions/PlayerInputActions::op_Implicit(PlayerActions/PlayerInputActions)
extern void PlayerInputActions_op_Implicit_mB2796EF06CEB98C9D2308C644398AB72D544DA0C (void);
// 0x0000001A System.Void PlayerActions/PlayerInputActions::SetCallbacks(PlayerActions/IPlayerInputActions)
extern void PlayerInputActions_SetCallbacks_m5F707BF059954205D04E758A831E47C7EFEE3586 (void);
// 0x0000001B System.Void PlayerActions/IPlayerInputActions::OnSelectObject(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000001C System.Void PlayerActions/IPlayerInputActions::OnRoll(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000001D System.Void AudioManager::Awake()
extern void AudioManager_Awake_m8138BCED4D692C83C95626A1A09AB46EA5205569 (void);
// 0x0000001E System.Void AudioManager::Play(System.String)
extern void AudioManager_Play_mB69D5512DD0ECB0B853CD681D2282DC78844DE4F (void);
// 0x0000001F System.Void AudioManager::.ctor()
extern void AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6 (void);
// 0x00000020 System.Void AudioManager/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m3DD23E7EFFDED69B9EAC81FE50CDC950F4B7EC9D (void);
// 0x00000021 System.Boolean AudioManager/<>c__DisplayClass4_0::<Play>b__0(Sound)
extern void U3CU3Ec__DisplayClass4_0_U3CPlayU3Eb__0_m2BD9F6F9E24D884441DC3F502D3BA0662509EFBF (void);
// 0x00000022 System.Void DiceManager::Update()
extern void DiceManager_Update_mD7BCA9BDEAEE68910F33618BE08BADD7AD97B025 (void);
// 0x00000023 System.Int32 DiceManager::GetUpSide()
extern void DiceManager_GetUpSide_mD889517AFB24AFDC999AC2817D26CAAB86B0D6CA (void);
// 0x00000024 System.Void DiceManager::.ctor()
extern void DiceManager__ctor_m20951FCCDDDA8142BDFD6093C61DD66BF4A877DE (void);
// 0x00000025 System.Void DiceRollManager::Awake()
extern void DiceRollManager_Awake_m76E10A02E54A027F566F7559FFED577C65BB79DB (void);
// 0x00000026 System.Void DiceRollManager::ResetToStartPosition()
extern void DiceRollManager_ResetToStartPosition_m14750CB86B45B2160778B2788E6B1976E3ADA769 (void);
// 0x00000027 System.Void DiceRollManager::Roll()
extern void DiceRollManager_Roll_m6A3D8AAB1F34375C59972C990DC15F5A56D88C00 (void);
// 0x00000028 System.Void DiceRollManager::FixedUpdate()
extern void DiceRollManager_FixedUpdate_m7CAD93D144A9469A847FE72F01E79F1406844595 (void);
// 0x00000029 System.Void DiceRollManager::Update()
extern void DiceRollManager_Update_m473822A49E615934BA290B5F4352EE1005ECE5E4 (void);
// 0x0000002A System.Void DiceRollManager::GetUpSide()
extern void DiceRollManager_GetUpSide_mD7C27ACF42E030212687EC93921C8F7A9112F98A (void);
// 0x0000002B System.Void DiceRollManager::.ctor()
extern void DiceRollManager__ctor_m73BCE8CC34A3927B97B7207B62C1D67286521BA2 (void);
// 0x0000002C System.Void Field::Awake()
extern void Field_Awake_m7875625BB28CA3AE01AD86212DB520D7F9713979 (void);
// 0x0000002D Field Field::FindNextField(System.Int32,System.Boolean)
extern void Field_FindNextField_m929CEE4C39B7FC3B121EE5B2B64B6E06988CA6AD (void);
// 0x0000002E Field Field::GetNextField(System.Boolean)
extern void Field_GetNextField_m58A1739581CB3F3716B532377FA44D2360728387 (void);
// 0x0000002F System.Void Field::Select()
extern void Field_Select_mB4BC453F055D323C607B6AF3BA920BAB1B7A9D58 (void);
// 0x00000030 System.Void Field::UnSelect()
extern void Field_UnSelect_mBBD5CC40868F0E20A426F28DE02EA7BCC636627C (void);
// 0x00000031 System.Void Field::Land()
extern void Field_Land_m3405DE49BD253EE818E8411B745612427267786D (void);
// 0x00000032 System.Void Field::SetActualFigure(Figure)
extern void Field_SetActualFigure_m3ED41503CD82850F8B4F7E117085EB3B24FB9D9B (void);
// 0x00000033 Figure Field::GetActualFigure()
extern void Field_GetActualFigure_mBFBCF64B0A3AA89A8D27C5DA5D022C7F7CF29A5E (void);
// 0x00000034 System.Void Field::Setselectable(System.Boolean)
extern void Field_Setselectable_m45BEAAC375FDEDCA4C571896A40F6B2C68D53CD0 (void);
// 0x00000035 System.Boolean Field::IsSelectable()
extern void Field_IsSelectable_mEEC5D46CC469B6A97EC3E09520BB4D6F6C51B838 (void);
// 0x00000036 UnityEngine.GameObject Field::GetObject()
extern void Field_GetObject_mC4982269CEB8B5AAC63AEB2B665E872CFF4C139A (void);
// 0x00000037 System.Void Field::SetNextField(Field)
extern void Field_SetNextField_m09FAE9F2D16249042589E5ABB6753B5BA76A1CFF (void);
// 0x00000038 System.Void Field::.ctor()
extern void Field__ctor_mBE0BDBDF4C0585A674EAE3F341CFB39975A59492 (void);
// 0x00000039 System.Void Figure::Awake()
extern void Figure_Awake_m7A9555EE9D8D21C768921A6E1E8388CFE588EBC4 (void);
// 0x0000003A System.Void Figure::GoToHome()
extern void Figure_GoToHome_mB669B734AC2310EE27C05DC10EC3E8D6CDE8796F (void);
// 0x0000003B System.Void Figure::SetHome()
extern void Figure_SetHome_mC0A394B775D44FB0F969782C7519A22913FACAB4 (void);
// 0x0000003C System.Void Figure::GotoNextField()
extern void Figure_GotoNextField_mBD3921ED266D99B8EE81355E2C8D392A4AF4598F (void);
// 0x0000003D System.Void Figure::FixedUpdate()
extern void Figure_FixedUpdate_m4295970820CF7FE40662CEDB0B524A3583C75028 (void);
// 0x0000003E System.Boolean Figure::CanGo(System.Int32)
extern void Figure_CanGo_mD2DEBDDB701E50041960D999C5F3C8E755F2E85B (void);
// 0x0000003F Field Figure::GetNextField(System.Int32)
extern void Figure_GetNextField_mD6CB4D63889DC27D8BD40DDD0E0D6E44B49D038D (void);
// 0x00000040 Field Figure::GetNextFieldToMove(Field)
extern void Figure_GetNextFieldToMove_m081887A5D77C3795205EA9C804431C32E38B89B0 (void);
// 0x00000041 Field Figure::GetNextField(System.Int32,Field)
extern void Figure_GetNextField_mA2E11B6A114C7AD2C85E88B82EB383E7B2610309 (void);
// 0x00000042 FigureAnimation Figure::GetAnimator()
extern void Figure_GetAnimator_mCF7718FCB0B2820281B6F9533B2DA3568A0C27DF (void);
// 0x00000043 System.Void Figure::Select(System.Int32)
extern void Figure_Select_m1A1FB0CAE99F06475CD65DC9F75B61ACF3641E40 (void);
// 0x00000044 System.Void Figure::Land()
extern void Figure_Land_m5865920EDD7EA2DE2B8310734CBD051EDC062122 (void);
// 0x00000045 System.Void Figure::Unselect()
extern void Figure_Unselect_m62774F8889847E0C8F9D771805D4043033A83355 (void);
// 0x00000046 Field Figure::GetStartField()
extern void Figure_GetStartField_m21FFC2AEFD1242E57328554DF32199B4D55A9689 (void);
// 0x00000047 Field Figure::GetActualField()
extern void Figure_GetActualField_m715BE409D32A467BBA725D745754573F5FE27D5D (void);
// 0x00000048 System.Boolean Figure::ItIsNextField(Field)
extern void Figure_ItIsNextField_mB192DB7438FB165E23309918D7ACE42A3E4B4C8E (void);
// 0x00000049 System.Boolean Figure::IsEnemy()
extern void Figure_IsEnemy_m5FA03C3DDDE8C9115AB1AF7AC8350CD6ED48DFED (void);
// 0x0000004A System.Boolean Figure::IsInHome()
extern void Figure_IsInHome_mB5AFFB11808371671E3CB8724763C410BAE43FE4 (void);
// 0x0000004B System.Void Figure::.ctor()
extern void Figure__ctor_mA6A360042EB0BD6AA45208E4568FBBBDAE8E9CCC (void);
// 0x0000004C System.Void FigureAnimation::Awake()
extern void FigureAnimation_Awake_mDD09B9795CBB4EFF128B27A15F8C241838AB63CA (void);
// 0x0000004D System.Void FigureAnimation::DyingSound()
extern void FigureAnimation_DyingSound_mE7E093C5C90F30C18B8B3552D9558B51CCDC4B05 (void);
// 0x0000004E System.Void FigureAnimation::DyingAnimation()
extern void FigureAnimation_DyingAnimation_m76281E6874B40D8CF9D27132F7D877138B344F54 (void);
// 0x0000004F System.Void FigureAnimation::GoHome()
extern void FigureAnimation_GoHome_mF7F6E6AA8D4CB3F36805163CA4968A71168AAF44 (void);
// 0x00000050 System.Void FigureAnimation::.ctor()
extern void FigureAnimation__ctor_m0C58BCECF3A6814B0D4162DD7F777B68ED008C04 (void);
// 0x00000051 System.Void GameOver::SetWintext(System.Boolean)
extern void GameOver_SetWintext_m0C4B5E526AB4EEDD9CE554A66EF7F36236C17625 (void);
// 0x00000052 System.Void GameOver::ResetLevel()
extern void GameOver_ResetLevel_mFF9BE7A6C094F59E4F7AD1AF17352EBDE069369D (void);
// 0x00000053 System.Void GameOver::.ctor()
extern void GameOver__ctor_m2D4239F9C4BCE2EBFD1D1D6FAFEBAD05F65399B2 (void);
// 0x00000054 System.Void GameUI::SwitchDiceType()
extern void GameUI_SwitchDiceType_mC38C67DDFF57220FE9127FFBD34BBC2832D67D06 (void);
// 0x00000055 System.Void GameUI::OpenMenu()
extern void GameUI_OpenMenu_mED950C0398761B827052DD36AAF66F7A9A0E4ECE (void);
// 0x00000056 System.Void GameUI::.ctor()
extern void GameUI__ctor_m2FD08F8B3345F733C4E363D20891F3EDC36A69CB (void);
// 0x00000057 System.Void MenuUI::Awake()
extern void MenuUI_Awake_m5FF98668F396F43AF9FD2F751F63859AB0F26E33 (void);
// 0x00000058 System.Void MenuUI::PlayGame()
extern void MenuUI_PlayGame_mBE0EEDBA032EB7B69EFBBB8916A425F92BB636E0 (void);
// 0x00000059 System.Void MenuUI::QuitGame()
extern void MenuUI_QuitGame_m424202CDF5618462D270B8E17274AB69684955B9 (void);
// 0x0000005A System.Void MenuUI::.ctor()
extern void MenuUI__ctor_mDA6058D5812A9E7D79C89502320AE6F4F07249E4 (void);
// 0x0000005B System.Void PlayerManager::Awake()
extern void PlayerManager_Awake_m4A997ABB547A7B4C3BA447681C1C8718B7B4A5B6 (void);
// 0x0000005C System.Void PlayerManager::TrySelectObject(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerManager_TrySelectObject_m0646122ADBF2913D54166B5B6F236E288A8431BB (void);
// 0x0000005D System.Boolean PlayerManager::SwitchDice()
extern void PlayerManager_SwitchDice_m33F6405CA3D989338C9A4B1AB669FADF5217D559 (void);
// 0x0000005E System.Void PlayerManager::WinControl()
extern void PlayerManager_WinControl_m635B529E197B7087B12BC83F883798D28276E7AC (void);
// 0x0000005F System.Void PlayerManager::MakeMove()
extern void PlayerManager_MakeMove_m088DA57F5417C436DE3941AABBB3672E4D3A6D39 (void);
// 0x00000060 System.Void PlayerManager::SetTossedNum(System.Int32)
extern void PlayerManager_SetTossedNum_m5D25660DB2BA96CD00372CA7877207EB9283ABD2 (void);
// 0x00000061 System.Collections.IEnumerator PlayerManager::PlayerRoll()
extern void PlayerManager_PlayerRoll_m580DFA9E486C008C9020BAEE42C12845C3BE8B4E (void);
// 0x00000062 System.Collections.IEnumerator PlayerManager::CamerTransitionBack()
extern void PlayerManager_CamerTransitionBack_m4FB6B2E8236B5F195B91B7AA327AA8561DCADAC9 (void);
// 0x00000063 System.Void PlayerManager::RollDice()
extern void PlayerManager_RollDice_m43BC5006F50E4EF672CB43CB84822A56653CFE62 (void);
// 0x00000064 System.Void PlayerManager::EnemyAI()
extern void PlayerManager_EnemyAI_mC3569E0EF302E820934E054676DE9C3C4223B47C (void);
// 0x00000065 System.Void PlayerManager::SetMenuIsOpen(System.Boolean)
extern void PlayerManager_SetMenuIsOpen_m779759A96A27F8C219D41E9A7BF3074CD012FEFE (void);
// 0x00000066 System.Void PlayerManager::CanPlay()
extern void PlayerManager_CanPlay_m4642076AD089DF1AD25E6631CB9DD77E8DDE224D (void);
// 0x00000067 System.Void PlayerManager::.ctor()
extern void PlayerManager__ctor_mEDEFFACDDE66D7077DE8CDED6560CA06218B5A0E (void);
// 0x00000068 System.Void PlayerManager/<PlayerRoll>d__27::.ctor(System.Int32)
extern void U3CPlayerRollU3Ed__27__ctor_mC7056D68F502EE93590490F9F491AEF1067D2AFD (void);
// 0x00000069 System.Void PlayerManager/<PlayerRoll>d__27::System.IDisposable.Dispose()
extern void U3CPlayerRollU3Ed__27_System_IDisposable_Dispose_mE3387C4AEF1D2C8FCDA3A02A5459FF9B62E2F490 (void);
// 0x0000006A System.Boolean PlayerManager/<PlayerRoll>d__27::MoveNext()
extern void U3CPlayerRollU3Ed__27_MoveNext_m80B1C7767B85B3A2F5C5542243981D37958320D6 (void);
// 0x0000006B System.Object PlayerManager/<PlayerRoll>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerRollU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5498693A55CD8C7F18255F5EA934D83C50503D4B (void);
// 0x0000006C System.Void PlayerManager/<PlayerRoll>d__27::System.Collections.IEnumerator.Reset()
extern void U3CPlayerRollU3Ed__27_System_Collections_IEnumerator_Reset_m5F760B68ECF256F8450E9F27262DE31EF26C122E (void);
// 0x0000006D System.Object PlayerManager/<PlayerRoll>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerRollU3Ed__27_System_Collections_IEnumerator_get_Current_m9A2076E1E7D772F6F9881AEFB562BE6A8D44506B (void);
// 0x0000006E System.Void PlayerManager/<CamerTransitionBack>d__28::.ctor(System.Int32)
extern void U3CCamerTransitionBackU3Ed__28__ctor_m2FAD5A39A377FE237B314E92D738472D3F68CF86 (void);
// 0x0000006F System.Void PlayerManager/<CamerTransitionBack>d__28::System.IDisposable.Dispose()
extern void U3CCamerTransitionBackU3Ed__28_System_IDisposable_Dispose_mEFD588071F560846C5BC96EE4677E69203139B2C (void);
// 0x00000070 System.Boolean PlayerManager/<CamerTransitionBack>d__28::MoveNext()
extern void U3CCamerTransitionBackU3Ed__28_MoveNext_m8C7DC8293E6DC2265975B56AE749CB7232C36D4E (void);
// 0x00000071 System.Object PlayerManager/<CamerTransitionBack>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCamerTransitionBackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00ECABF504AEC69BCB185F95F696875B9887CAC9 (void);
// 0x00000072 System.Void PlayerManager/<CamerTransitionBack>d__28::System.Collections.IEnumerator.Reset()
extern void U3CCamerTransitionBackU3Ed__28_System_Collections_IEnumerator_Reset_m09E91627174D88B98B76903F9DC638288211D771 (void);
// 0x00000073 System.Object PlayerManager/<CamerTransitionBack>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CCamerTransitionBackU3Ed__28_System_Collections_IEnumerator_get_Current_mA6C7A22E4B1C6804F2E183447B01BCF0791063B0 (void);
// 0x00000074 System.Void RandomAnimationField::Awake()
extern void RandomAnimationField_Awake_mEB5A2E533AF0B1BACDF1E1057179ED021F7BB12A (void);
// 0x00000075 System.Void RandomAnimationField::.ctor()
extern void RandomAnimationField__ctor_m1B5B345C2F816D52C85114BC25832F985A01245C (void);
// 0x00000076 System.Void RandomField::Awake()
extern void RandomField_Awake_mD646ABEF6534CAA57531A7FD7849CD7FC0CE29BD (void);
// 0x00000077 System.Void RandomField::.ctor()
extern void RandomField__ctor_m2D2AB74CB7518A0AA8A4CD2B1A3E0273B7CE9A47 (void);
// 0x00000078 System.Void Sound::.ctor()
extern void Sound__ctor_m5DD7C9F71B98D5670BBDD05F6D6FCDF43DC9EA8F (void);
// 0x00000079 System.Void SparkExecutor::OnTriggerEnter(UnityEngine.Collider)
extern void SparkExecutor_OnTriggerEnter_mDE009A47B5F27E6ECFF583B400AA263D664D5C78 (void);
// 0x0000007A System.Void SparkExecutor::.ctor()
extern void SparkExecutor__ctor_mDC50D34478F4254969227D247BD3B6019EFB60BD (void);
// 0x0000007B System.Void Readme::.ctor()
extern void Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6 (void);
// 0x0000007C System.Void Readme/Section::.ctor()
extern void Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B (void);
static Il2CppMethodPointer s_methodPointers[124] = 
{
	PlayerActions_get_asset_m29F00B42636E9B80C6DF87F9A9B83C9C881FCF36,
	PlayerActions__ctor_m71B1566835E2A2243BA0163398A8130FADFF3E63,
	PlayerActions_Dispose_mE7319569DEEED044CEF4B5193AF0B2F13633D733,
	PlayerActions_get_bindingMask_m76272F21A20F33C2510D9D699CEE7A16DF3F6D78,
	PlayerActions_set_bindingMask_m557941BC7F5C3BD7EC83E28A2EC7FCDF2CC10D98,
	PlayerActions_get_devices_m21C9DB4AC617CA00A5EDFE4F2067CFC3A383E9F2,
	PlayerActions_set_devices_m46D23CC268E0ACCA7153C01E0B24AD2BD5033305,
	PlayerActions_get_controlSchemes_m100DBBF22D804B3F8A7E63B51A72ACF585D486A4,
	PlayerActions_Contains_m1996C81A1DEC87153D96F1A7F5E95B0286543375,
	PlayerActions_GetEnumerator_m64E04259CC1A157904C654A95B8F0448A2F5D409,
	PlayerActions_System_Collections_IEnumerable_GetEnumerator_m147E7F0824387F22086B7E30AE31AC17379B6E77,
	PlayerActions_Enable_mB26A7C4A622706BA8A679BD26EEB7A2B946881DA,
	PlayerActions_Disable_m069852B96E458E70A89447C30705DF2766E11D36,
	PlayerActions_get_bindings_m74298D95C84A686A43A353FB95FC1D81D2C5F87A,
	PlayerActions_FindAction_mE71FBCB5FF45543112F0F49E28A792D08E6EC8AA,
	PlayerActions_FindBinding_m725102DA2718F391A9436716D41E7B14CEB0EDC4,
	PlayerActions_get_PlayerInput_m496463FD573EA2247A563463628B6D1F6E47127F,
	PlayerInputActions__ctor_mD110058A0B99BFDE4CDCD9AFE8027AE867EA0821,
	PlayerInputActions_get_SelectObject_m86FF3A77E62AB507B68E37AEB25458C51BBE48B4,
	PlayerInputActions_get_Roll_mE3BEACD52114FB801E4BB71A7967C4EB20F92F68,
	PlayerInputActions_Get_m2E2FD12DD34A0E939840DCE0C355B82A58EAD3D1,
	PlayerInputActions_Enable_m8EDBCF1082E3F8F4EF0D41AFBE7F25A0B4A44AEA,
	PlayerInputActions_Disable_m4DB32972E104D79F5C70A876F62CC3D4E0182741,
	PlayerInputActions_get_enabled_mE88F22F42229857B001649C56D27581399997BDA,
	PlayerInputActions_op_Implicit_mB2796EF06CEB98C9D2308C644398AB72D544DA0C,
	PlayerInputActions_SetCallbacks_m5F707BF059954205D04E758A831E47C7EFEE3586,
	NULL,
	NULL,
	AudioManager_Awake_m8138BCED4D692C83C95626A1A09AB46EA5205569,
	AudioManager_Play_mB69D5512DD0ECB0B853CD681D2282DC78844DE4F,
	AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6,
	U3CU3Ec__DisplayClass4_0__ctor_m3DD23E7EFFDED69B9EAC81FE50CDC950F4B7EC9D,
	U3CU3Ec__DisplayClass4_0_U3CPlayU3Eb__0_m2BD9F6F9E24D884441DC3F502D3BA0662509EFBF,
	DiceManager_Update_mD7BCA9BDEAEE68910F33618BE08BADD7AD97B025,
	DiceManager_GetUpSide_mD889517AFB24AFDC999AC2817D26CAAB86B0D6CA,
	DiceManager__ctor_m20951FCCDDDA8142BDFD6093C61DD66BF4A877DE,
	DiceRollManager_Awake_m76E10A02E54A027F566F7559FFED577C65BB79DB,
	DiceRollManager_ResetToStartPosition_m14750CB86B45B2160778B2788E6B1976E3ADA769,
	DiceRollManager_Roll_m6A3D8AAB1F34375C59972C990DC15F5A56D88C00,
	DiceRollManager_FixedUpdate_m7CAD93D144A9469A847FE72F01E79F1406844595,
	DiceRollManager_Update_m473822A49E615934BA290B5F4352EE1005ECE5E4,
	DiceRollManager_GetUpSide_mD7C27ACF42E030212687EC93921C8F7A9112F98A,
	DiceRollManager__ctor_m73BCE8CC34A3927B97B7207B62C1D67286521BA2,
	Field_Awake_m7875625BB28CA3AE01AD86212DB520D7F9713979,
	Field_FindNextField_m929CEE4C39B7FC3B121EE5B2B64B6E06988CA6AD,
	Field_GetNextField_m58A1739581CB3F3716B532377FA44D2360728387,
	Field_Select_mB4BC453F055D323C607B6AF3BA920BAB1B7A9D58,
	Field_UnSelect_mBBD5CC40868F0E20A426F28DE02EA7BCC636627C,
	Field_Land_m3405DE49BD253EE818E8411B745612427267786D,
	Field_SetActualFigure_m3ED41503CD82850F8B4F7E117085EB3B24FB9D9B,
	Field_GetActualFigure_mBFBCF64B0A3AA89A8D27C5DA5D022C7F7CF29A5E,
	Field_Setselectable_m45BEAAC375FDEDCA4C571896A40F6B2C68D53CD0,
	Field_IsSelectable_mEEC5D46CC469B6A97EC3E09520BB4D6F6C51B838,
	Field_GetObject_mC4982269CEB8B5AAC63AEB2B665E872CFF4C139A,
	Field_SetNextField_m09FAE9F2D16249042589E5ABB6753B5BA76A1CFF,
	Field__ctor_mBE0BDBDF4C0585A674EAE3F341CFB39975A59492,
	Figure_Awake_m7A9555EE9D8D21C768921A6E1E8388CFE588EBC4,
	Figure_GoToHome_mB669B734AC2310EE27C05DC10EC3E8D6CDE8796F,
	Figure_SetHome_mC0A394B775D44FB0F969782C7519A22913FACAB4,
	Figure_GotoNextField_mBD3921ED266D99B8EE81355E2C8D392A4AF4598F,
	Figure_FixedUpdate_m4295970820CF7FE40662CEDB0B524A3583C75028,
	Figure_CanGo_mD2DEBDDB701E50041960D999C5F3C8E755F2E85B,
	Figure_GetNextField_mD6CB4D63889DC27D8BD40DDD0E0D6E44B49D038D,
	Figure_GetNextFieldToMove_m081887A5D77C3795205EA9C804431C32E38B89B0,
	Figure_GetNextField_mA2E11B6A114C7AD2C85E88B82EB383E7B2610309,
	Figure_GetAnimator_mCF7718FCB0B2820281B6F9533B2DA3568A0C27DF,
	Figure_Select_m1A1FB0CAE99F06475CD65DC9F75B61ACF3641E40,
	Figure_Land_m5865920EDD7EA2DE2B8310734CBD051EDC062122,
	Figure_Unselect_m62774F8889847E0C8F9D771805D4043033A83355,
	Figure_GetStartField_m21FFC2AEFD1242E57328554DF32199B4D55A9689,
	Figure_GetActualField_m715BE409D32A467BBA725D745754573F5FE27D5D,
	Figure_ItIsNextField_mB192DB7438FB165E23309918D7ACE42A3E4B4C8E,
	Figure_IsEnemy_m5FA03C3DDDE8C9115AB1AF7AC8350CD6ED48DFED,
	Figure_IsInHome_mB5AFFB11808371671E3CB8724763C410BAE43FE4,
	Figure__ctor_mA6A360042EB0BD6AA45208E4568FBBBDAE8E9CCC,
	FigureAnimation_Awake_mDD09B9795CBB4EFF128B27A15F8C241838AB63CA,
	FigureAnimation_DyingSound_mE7E093C5C90F30C18B8B3552D9558B51CCDC4B05,
	FigureAnimation_DyingAnimation_m76281E6874B40D8CF9D27132F7D877138B344F54,
	FigureAnimation_GoHome_mF7F6E6AA8D4CB3F36805163CA4968A71168AAF44,
	FigureAnimation__ctor_m0C58BCECF3A6814B0D4162DD7F777B68ED008C04,
	GameOver_SetWintext_m0C4B5E526AB4EEDD9CE554A66EF7F36236C17625,
	GameOver_ResetLevel_mFF9BE7A6C094F59E4F7AD1AF17352EBDE069369D,
	GameOver__ctor_m2D4239F9C4BCE2EBFD1D1D6FAFEBAD05F65399B2,
	GameUI_SwitchDiceType_mC38C67DDFF57220FE9127FFBD34BBC2832D67D06,
	GameUI_OpenMenu_mED950C0398761B827052DD36AAF66F7A9A0E4ECE,
	GameUI__ctor_m2FD08F8B3345F733C4E363D20891F3EDC36A69CB,
	MenuUI_Awake_m5FF98668F396F43AF9FD2F751F63859AB0F26E33,
	MenuUI_PlayGame_mBE0EEDBA032EB7B69EFBBB8916A425F92BB636E0,
	MenuUI_QuitGame_m424202CDF5618462D270B8E17274AB69684955B9,
	MenuUI__ctor_mDA6058D5812A9E7D79C89502320AE6F4F07249E4,
	PlayerManager_Awake_m4A997ABB547A7B4C3BA447681C1C8718B7B4A5B6,
	PlayerManager_TrySelectObject_m0646122ADBF2913D54166B5B6F236E288A8431BB,
	PlayerManager_SwitchDice_m33F6405CA3D989338C9A4B1AB669FADF5217D559,
	PlayerManager_WinControl_m635B529E197B7087B12BC83F883798D28276E7AC,
	PlayerManager_MakeMove_m088DA57F5417C436DE3941AABBB3672E4D3A6D39,
	PlayerManager_SetTossedNum_m5D25660DB2BA96CD00372CA7877207EB9283ABD2,
	PlayerManager_PlayerRoll_m580DFA9E486C008C9020BAEE42C12845C3BE8B4E,
	PlayerManager_CamerTransitionBack_m4FB6B2E8236B5F195B91B7AA327AA8561DCADAC9,
	PlayerManager_RollDice_m43BC5006F50E4EF672CB43CB84822A56653CFE62,
	PlayerManager_EnemyAI_mC3569E0EF302E820934E054676DE9C3C4223B47C,
	PlayerManager_SetMenuIsOpen_m779759A96A27F8C219D41E9A7BF3074CD012FEFE,
	PlayerManager_CanPlay_m4642076AD089DF1AD25E6631CB9DD77E8DDE224D,
	PlayerManager__ctor_mEDEFFACDDE66D7077DE8CDED6560CA06218B5A0E,
	U3CPlayerRollU3Ed__27__ctor_mC7056D68F502EE93590490F9F491AEF1067D2AFD,
	U3CPlayerRollU3Ed__27_System_IDisposable_Dispose_mE3387C4AEF1D2C8FCDA3A02A5459FF9B62E2F490,
	U3CPlayerRollU3Ed__27_MoveNext_m80B1C7767B85B3A2F5C5542243981D37958320D6,
	U3CPlayerRollU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5498693A55CD8C7F18255F5EA934D83C50503D4B,
	U3CPlayerRollU3Ed__27_System_Collections_IEnumerator_Reset_m5F760B68ECF256F8450E9F27262DE31EF26C122E,
	U3CPlayerRollU3Ed__27_System_Collections_IEnumerator_get_Current_m9A2076E1E7D772F6F9881AEFB562BE6A8D44506B,
	U3CCamerTransitionBackU3Ed__28__ctor_m2FAD5A39A377FE237B314E92D738472D3F68CF86,
	U3CCamerTransitionBackU3Ed__28_System_IDisposable_Dispose_mEFD588071F560846C5BC96EE4677E69203139B2C,
	U3CCamerTransitionBackU3Ed__28_MoveNext_m8C7DC8293E6DC2265975B56AE749CB7232C36D4E,
	U3CCamerTransitionBackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00ECABF504AEC69BCB185F95F696875B9887CAC9,
	U3CCamerTransitionBackU3Ed__28_System_Collections_IEnumerator_Reset_m09E91627174D88B98B76903F9DC638288211D771,
	U3CCamerTransitionBackU3Ed__28_System_Collections_IEnumerator_get_Current_mA6C7A22E4B1C6804F2E183447B01BCF0791063B0,
	RandomAnimationField_Awake_mEB5A2E533AF0B1BACDF1E1057179ED021F7BB12A,
	RandomAnimationField__ctor_m1B5B345C2F816D52C85114BC25832F985A01245C,
	RandomField_Awake_mD646ABEF6534CAA57531A7FD7849CD7FC0CE29BD,
	RandomField__ctor_m2D2AB74CB7518A0AA8A4CD2B1A3E0273B7CE9A47,
	Sound__ctor_m5DD7C9F71B98D5670BBDD05F6D6FCDF43DC9EA8F,
	SparkExecutor_OnTriggerEnter_mDE009A47B5F27E6ECFF583B400AA263D664D5C78,
	SparkExecutor__ctor_mDC50D34478F4254969227D247BD3B6019EFB60BD,
	Readme__ctor_m69C325C4C171DCB0312B646A9034AA91EA8C39C6,
	Section__ctor_m5F732533E4DFC0167D965E5F5DB332E46055399B,
};
extern void PlayerInputActions__ctor_mD110058A0B99BFDE4CDCD9AFE8027AE867EA0821_AdjustorThunk (void);
extern void PlayerInputActions_get_SelectObject_m86FF3A77E62AB507B68E37AEB25458C51BBE48B4_AdjustorThunk (void);
extern void PlayerInputActions_get_Roll_mE3BEACD52114FB801E4BB71A7967C4EB20F92F68_AdjustorThunk (void);
extern void PlayerInputActions_Get_m2E2FD12DD34A0E939840DCE0C355B82A58EAD3D1_AdjustorThunk (void);
extern void PlayerInputActions_Enable_m8EDBCF1082E3F8F4EF0D41AFBE7F25A0B4A44AEA_AdjustorThunk (void);
extern void PlayerInputActions_Disable_m4DB32972E104D79F5C70A876F62CC3D4E0182741_AdjustorThunk (void);
extern void PlayerInputActions_get_enabled_mE88F22F42229857B001649C56D27581399997BDA_AdjustorThunk (void);
extern void PlayerInputActions_SetCallbacks_m5F707BF059954205D04E758A831E47C7EFEE3586_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000012, PlayerInputActions__ctor_mD110058A0B99BFDE4CDCD9AFE8027AE867EA0821_AdjustorThunk },
	{ 0x06000013, PlayerInputActions_get_SelectObject_m86FF3A77E62AB507B68E37AEB25458C51BBE48B4_AdjustorThunk },
	{ 0x06000014, PlayerInputActions_get_Roll_mE3BEACD52114FB801E4BB71A7967C4EB20F92F68_AdjustorThunk },
	{ 0x06000015, PlayerInputActions_Get_m2E2FD12DD34A0E939840DCE0C355B82A58EAD3D1_AdjustorThunk },
	{ 0x06000016, PlayerInputActions_Enable_m8EDBCF1082E3F8F4EF0D41AFBE7F25A0B4A44AEA_AdjustorThunk },
	{ 0x06000017, PlayerInputActions_Disable_m4DB32972E104D79F5C70A876F62CC3D4E0182741_AdjustorThunk },
	{ 0x06000018, PlayerInputActions_get_enabled_mE88F22F42229857B001649C56D27581399997BDA_AdjustorThunk },
	{ 0x0600001A, PlayerInputActions_SetCallbacks_m5F707BF059954205D04E758A831E47C7EFEE3586_AdjustorThunk },
};
static const int32_t s_InvokerIndices[124] = 
{
	6868,
	7001,
	7001,
	6664,
	5323,
	6662,
	5321,
	6673,
	3894,
	6868,
	6868,
	7001,
	7001,
	6868,
	2387,
	2121,
	7124,
	5519,
	6868,
	6868,
	6868,
	7001,
	7001,
	6762,
	11317,
	5519,
	0,
	0,
	7001,
	5519,
	7001,
	7001,
	3894,
	7001,
	6838,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	2376,
	4892,
	7001,
	7001,
	7001,
	5519,
	6868,
	5411,
	6762,
	6868,
	5519,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	3861,
	4899,
	4906,
	2379,
	6868,
	5489,
	7001,
	7001,
	6868,
	6868,
	3894,
	6762,
	6762,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	5411,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	7001,
	5766,
	6762,
	7001,
	7001,
	5489,
	6868,
	6868,
	7001,
	7001,
	5411,
	7001,
	7001,
	5489,
	7001,
	6762,
	6868,
	7001,
	6868,
	5489,
	7001,
	6762,
	6868,
	7001,
	6868,
	7001,
	7001,
	7001,
	7001,
	7001,
	5519,
	7001,
	7001,
	7001,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	124,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
