using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Field))]
public class FieldEditor : Editor {

    public int NumOfSpawn;
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        Field field = (Field)target;
        GUILayout.Space(10);
        if(GUILayout.Button("Create linked Field")) {
            field.CreateNextField(field.numOfNextField, 0);
        }

        if(GUILayout.Button("Rotate Field on next Field")) {
            field.RotateOnNextField();
        }
    }
}
