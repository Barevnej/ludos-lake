using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour {
    [Header("Figure Elements")]
    [Range(10f, 50f)]
    [Tooltip("The speed value should be between 10 and 50. It sets the speed of figure movements.")]
    [SerializeField] private float speed = 20;
    [Tooltip("This curve is used for the specification of the figure jump trajectory.")]
    [SerializeField] private AnimationCurve curve;
    [Tooltip("Figure component.")]
    [SerializeField] private GameObject meshObject;
    [Tooltip("Figure component.")]
    [SerializeField] private FigureAnimation animation;
    [Tooltip("Type of figure.")]
    [SerializeField] private bool _enemy;

    [Space(10)]
    [Header("Fields")]
    [Tooltip("The field where the figure standing.")]
    [SerializeField] private Field _actualField;
    [Tooltip("Home field of the figure.")]
    [SerializeField] private Field startField;

    [Space(10)]
    [Header("Player")]
    [Tooltip("Link to game logic.")]
    [SerializeField] private PlayerManager playerManager;

    private Material material;
    private Field nextField;
    private Color originalColor;
    private Field _homefield;
    private bool _isInHome;

    private Vector3 curentPosition;
    private Vector3 targetPosition;
    private Quaternion currentRotation;
    private Quaternion targetRotation;


    private Field tmpField;
    private bool going;

    private float timeToPosition;
    private float elapsedTime;

    private void Awake() {
        transform.position = new Vector3(_actualField.transform.position.x, _actualField.transform.position.y + 0.75f, _actualField.transform.position.z);
        transform.rotation = _actualField.transform.rotation;
        //animation.SetIdleAnimation(_actualField.GetObject());
        //transform.SetParent(_actualField.GetObject().transform, true);
        _homefield = _actualField;
        _isInHome = true;
        material = meshObject.GetComponent<MeshRenderer>().material;
        originalColor = material.GetColor("_BaseColor");
        tmpField = null;
    }

    public void GoToHome() {
        transform.position = new Vector3(_homefield.transform.position.x, _homefield.transform.position.y + 0.75f, _homefield.transform.position.z);
        transform.rotation = _homefield.transform.rotation;

    }

    public void SetHome() {
        _actualField.SetActualFigure(null);
        _actualField = _homefield;
        _homefield.SetActualFigure(this);
        _isInHome = true;
    }

    public void GotoNextField() {
        if(tmpField == null) {
            //transform.SetParent(null, true);
            tmpField = _actualField;
        }

        tmpField = GetNextFieldToMove(tmpField);
        if(tmpField == null)
            tmpField = nextField;
        //Debug.Log("Next field is: " + nextField.name + " Tmp field is: " + tmpField.name);

        _isInHome = false;
        
        if(nextField == tmpField && nextField.GetActualFigure() != null) {
            Figure tmp = nextField.GetActualFigure();
            tmp.SetHome();
            tmp.GetAnimator().DyingAnimation();
        }

        curentPosition = transform.position;
        currentRotation = transform.rotation;

        targetRotation = tmpField.transform.rotation;
        if(nextField == tmpField)
            targetPosition = new Vector3(tmpField.transform.position.x, tmpField.transform.position.y + 0.75f, tmpField.transform.position.z);
        else
            targetPosition = new Vector3(tmpField.transform.position.x, tmpField.transform.position.y + 5.75f, tmpField.transform.position.z);

        elapsedTime = 0;
        timeToPosition = Vector3.Distance(curentPosition, targetPosition)/speed;

        going = true;
    }


    private void FixedUpdate() {
        if(going) {
            elapsedTime += Time.fixedDeltaTime;
            float percentage = elapsedTime / timeToPosition;
            percentage = Mathf.SmoothStep(0, 1, percentage);
            
            transform.position = Vector3.Lerp(curentPosition, targetPosition, percentage);
            transform.rotation = Quaternion.Lerp(currentRotation, targetRotation, percentage);
            transform.position = new Vector3(transform.position.x, transform.position.y + curve.Evaluate(percentage), transform.position.z);

            if(percentage >= 1) {
                transform.position = targetPosition;
                transform.rotation = tmpField.transform.rotation;
                going = false;
                if(nextField == tmpField) {
                    _actualField.SetActualFigure(null);
                    _actualField = nextField;
                    //animation.SetIdleAnimation(_actualField.GetObject());
                    //transform.SetParent(_actualField.GetObject().transform, true);
                    _actualField.SetActualFigure(this);
                    tmpField = null;
                    Land();
                    playerManager.CanPlay();
                } else {
                    GotoNextField();
                }
            }
        }
    }

    public bool CanGo(int tossedNum) {
        return GetNextField(tossedNum) != null;
    }

    public Field GetNextField(int tossedNum) {
        return GetNextField(tossedNum, _actualField);
    }

    public Field GetNextFieldToMove(Field field) {
        if(_isInHome)
            return startField;
        return field.GetNextField(_enemy);
    }

    public Field GetNextField(int tossedNum, Field field) {
        if(_isInHome) {
            if(tossedNum == 6) {
                Figure tmp = startField.GetActualFigure();
                if(tmp != null && tmp.IsEnemy().Equals(_enemy))
                    return null;
                return startField;
            } else {
                return null;
            }
        }
        return field.FindNextField(tossedNum, _enemy);
    }

    public FigureAnimation GetAnimator() {
        return animation;
    }

    public void Select(int tossedNum) {
        material.SetFloat("_BlendOpacity", 10);
        nextField = GetNextField(tossedNum);
        if(nextField != null)
            nextField.Select();
    }

    public void Land() {
        material.SetFloat("_BlendOpacity", 0.3f);
        if(nextField != null)
            nextField.Land();
    }

    public void Unselect() {
        material.SetFloat("_BlendOpacity", 0.3f);
        if(nextField != null)
            nextField.UnSelect();
    }

    public Field GetStartField() {
        return startField;
    }

    public Field GetActualField() {
        return _actualField;
    }

    public bool ItIsNextField(Field tmp) {
        return (nextField == tmp);
    }

    public bool IsEnemy() {
        return _enemy;
    }

    public bool IsInHome() {
        return _isInHome;
    }

}
