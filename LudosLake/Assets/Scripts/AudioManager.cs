using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour {
    public Sound[] sounds;
    [SerializeField] private bool SpacialBlend;
    public static AudioManager audioManager;
    // Start is called before the first frame update
    void Awake() {
        if(audioManager != null && audioManager != this) {
            Destroy(this.gameObject);
            return;
        }

        audioManager = this;

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.spatialBlend = s.SpatialBlend;
        }
    }

    // Update is called once per frame
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

}
