using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using Cinemachine;

public class PlayerManager : MonoBehaviour {
    private enum GameFaze {
        DiceToss,
        Selection
    }

    [Header("Masks")]
    [Tooltip("Mask used to resolve field.")]
    [SerializeField] private LayerMask fieldMask;
    [Tooltip("Mask used to resolve figure.")]
    [SerializeField] private LayerMask figureMask;
    [Tooltip("Mask used to resolve UI.")]
    [SerializeField] private LayerMask uiMask;

    [Space(10)]
    [Header("UI Link")]
    [Tooltip("UI screen link.")]
    [SerializeField] private GameObject gameUI, gameOver;
    [Tooltip("Link to number in UI.")]
    [SerializeField] private TextMeshProUGUI tossedNumText;

    [Space(10)]
    [Header("Figures")]
    [Tooltip("Set of all player's figures.")]
    [SerializeField] private List<Figure> playerFigures;
    [Tooltip("Set of all enemy's figures.")]
    [SerializeField] private List<Figure> enemyFigures;

    [Space(10)]
    [Header("Scene Objects")]
    [Tooltip("Link on virtual camera which looks at dice.")]
    [SerializeField] private CinemachineVirtualCamera diceCamera;
    [Tooltip("Link on Dice for tossing.")]
    [SerializeField] private DiceRollManager dice;

    [Space(10)]
    [SerializeField] private bool enemyTurn;

    private bool physicalRoll;
    private int tossedNum;
    private GameFaze gameFaze;
    private PlayerActions inputs;
    private Camera _camera;
    private Figure figure;
    private bool canPlay;
    private bool gameIsOver;
    private bool menuIsOpen;

    private void Awake() {
        _camera = Camera.main;
        inputs = new PlayerActions();
        inputs.PlayerInput.SelectObject.performed += TrySelectObject;
        inputs.Enable();
        gameFaze = GameFaze.DiceToss;
        canPlay = true;
        physicalRoll= true;
        menuIsOpen = true;
    }

    private void TrySelectObject(InputAction.CallbackContext obj) {
        
        if(enemyTurn || !canPlay || gameIsOver || menuIsOpen) return;
            
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out RaycastHit testUI, 1000f, uiMask))
            return;

        switch(gameFaze) {
            case GameFaze.DiceToss:
                if(physicalRoll) {
                    StartCoroutine(PlayerRoll());
                    return;
                }
                RollDice();
                break;
            case GameFaze.Selection:
                if(Physics.Raycast(ray, out RaycastHit figureHit, 1000f, figureMask)) {
                    Figure tmp = figureHit.transform.parent.transform.parent.GetComponent<Figure>();

                    if(!enemyTurn.Equals(tmp.IsEnemy()) && tmp.GetActualField().IsSelectable()) {
                        MakeMove();
                        break;
                    }

                    if(figure != null)
                        figure.Unselect();

                    figure = tmp;
                    figure.Select(tossedNum);
                }
                
                if(figure != null) {
                    if(Physics.Raycast(ray, out RaycastHit fieldHit, 1000f, fieldMask)) {
                        Field tmpField = fieldHit.transform.parent.transform.parent.GetComponent<Field>();
                        Debug.Log("This is field");
                        if(figure.ItIsNextField(tmpField)) {
                            MakeMove();
                        }
                    }
                }
                break;
        }

        if(enemyTurn && !gameIsOver)
            EnemyAI();

    }

    public bool SwitchDice() {
        physicalRoll = !physicalRoll;
        return physicalRoll;
    }

    private void WinControl() {
        bool playerWin = true;
        bool enemyWin = true;

        foreach(Figure f in enemyFigures) {
            if(f.GetActualField().tag != "FinishField") {
                enemyWin = false;
                break;
            }
        }

        foreach(Figure f in playerFigures) {
            if(f.GetActualField().tag != "FinishField") {
                playerWin = false;
                break;
            }
        }

        if(enemyWin || playerWin) {
            gameIsOver = true;
            gameUI.SetActive(false);
            gameOver.SetActive(true);
        }

        if(enemyWin) {
            gameOver.GetComponent<GameOver>().SetWintext(false);
            AudioManager.audioManager.Play("Lose");
        } else if(playerWin) {
            gameOver.GetComponent<GameOver>().SetWintext(true);
            AudioManager.audioManager.Play("Win");
        }
    }

    private void MakeMove() {
        canPlay = false;
        figure.GotoNextField();
        figure = null;
        gameFaze = GameFaze.DiceToss;
        if(tossedNum != 6) {
            enemyTurn = !enemyTurn;
        }
    }

    

    public void SetTossedNum(int num) {
        diceCamera.Priority = 0;
        tossedNum = num;
        tossedNumText.text = tossedNum.ToString();
        tossedNumText.GetComponent<Animator>().SetTrigger("NumChange");
        tossedNumText.GetComponent<AudioSource>().Play();

        if(enemyTurn) {
            foreach(Figure f in enemyFigures) {
                if(f.CanGo(tossedNum)) {
                    gameFaze = GameFaze.Selection;
                    break;
                }
            }
        } else {
            foreach(Figure f in playerFigures) {
                if(f.CanGo(tossedNum)) {
                    gameFaze = GameFaze.Selection;
                    break;
                }
            }
        }

        if(gameFaze != GameFaze.Selection)
            enemyTurn = !enemyTurn;

        StartCoroutine(CamerTransitionBack());
    }
    IEnumerator PlayerRoll() {
            diceCamera.Priority = 2;
            dice.ResetToStartPosition();
            canPlay = false;
            yield return new WaitForSeconds(0.9f);
            dice.Roll();
    }
    IEnumerator CamerTransitionBack() {
        yield return new WaitForSeconds(0.9f);
        canPlay = true;
        if(enemyTurn)
            EnemyAI();
    }

    private void RollDice() {
        tossedNum = UnityEngine.Random.Range(1, 7);
        if(!enemyTurn) {
            tossedNumText.text = tossedNum.ToString();
            tossedNumText.GetComponent<Animator>().SetTrigger("NumChange");
            tossedNumText.GetComponent<AudioSource>().Play();
        }
        if(enemyTurn) {
            foreach(Figure f in enemyFigures) {
                if(f.CanGo(tossedNum)) {
                    gameFaze = GameFaze.Selection;
                    break;
                }
            }
        } else {
            foreach(Figure f in playerFigures) {
                if(f.CanGo(tossedNum)) {
                    gameFaze = GameFaze.Selection;
                    break;
                }
            }
        }

        if(gameFaze != GameFaze.Selection)
            enemyTurn = !enemyTurn;
    }

    private void EnemyAI() {
        if(!canPlay)
            return;

        RollDice();
        if(!enemyTurn)
            return;

        figure = null;
        int priority = 0;

        foreach(Figure f in enemyFigures) {
            if(f.CanGo(tossedNum)) {
                Field tmp = f.GetNextField(tossedNum);

                if(f.IsInHome()) {
                    figure = f;
                    break;
                }

                if(tmp.tag == "FinishField" && f.GetActualField().tag != "FinishField") {
                    priority = 4;
                    figure = f;
                }

                if(tmp.GetActualFigure() != null && priority < 3) {
                    priority = 3;
                    figure = f;
                }

                if(f.GetActualField() == f.GetStartField() && priority < 2) {
                    priority = 2;
                    figure = f;
                }

                if(priority < 1) {
                    priority = 1;
                    figure = f; 
                }
            }
        }

        figure.Select(tossedNum);

        MakeMove();

        if(enemyTurn && !gameIsOver)
            EnemyAI();
    }


    public void SetMenuIsOpen(bool a) {
        menuIsOpen = a;
    }

    public void CanPlay() {
        canPlay = true;
        WinControl();
        if(enemyTurn)
            EnemyAI();
    }

}
