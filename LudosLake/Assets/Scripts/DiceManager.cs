using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceManager : MonoBehaviour {
    [SerializeField] private List<Transform> sides;

    void Update() {
        

    }

    public int GetUpSide(){
        int upSide = 1;
        float yWorldPosition = sides[0].position.y;
        int i = 0;

        foreach(Transform t in sides) {
            i++;
            if(yWorldPosition < t.position.y) {
                yWorldPosition = t.position.y;
                upSide = i;
            }
        }
        return upSide;
    }
}
