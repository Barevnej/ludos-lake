using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUI : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI diceButtonText;
    [SerializeField] private GameObject menuUI;
    [SerializeField] private PlayerManager player;
    public void SwitchDiceType() {
        bool type = player.SwitchDice();
        if(type) {
            diceButtonText.text = "Physical Dice";
        } else {
            diceButtonText.text = "Random Dice";
        }
    }

    public void OpenMenu() {
        Time.timeScale = 0;
        menuUI.SetActive(true);
        player.SetMenuIsOpen(true);
        gameObject.SetActive(false);
    }
}
