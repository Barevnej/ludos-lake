using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimationField : MonoBehaviour
{
    private void Awake() {
        GetComponent<Animator>().Play("FieldAnimator", -1, Random.Range(0.0f, 1.0f));
        GetComponent<MeshRenderer>().material.SetVector("_NoiseOffset", new Vector2(Random.Range(0.0f, 50.0f), Random.Range(0.0f, 50.0f)));
    }
}
