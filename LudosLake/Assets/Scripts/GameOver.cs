using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour
{
    [Tooltip("UI text that appears after a win or loses.")]
    [SerializeField] private TextMeshProUGUI winText;


    public void SetWintext(bool a) {
        if(a) {
            winText.text = "PLAYER WON";
        } else {
            winText.text = "ENEMY WON";
        }
    }

    public void ResetLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
