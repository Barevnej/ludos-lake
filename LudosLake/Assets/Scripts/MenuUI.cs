using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUI : MonoBehaviour {
    [SerializeField] private GameObject gameUI;
    [SerializeField] private PlayerManager player;

    private void Awake() {
        Time.timeScale = 0;
    }
    public void PlayGame() {
        Time.timeScale = 1;
        gameUI.SetActive(true);
        player.SetMenuIsOpen(false);
        gameObject.SetActive(false);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
