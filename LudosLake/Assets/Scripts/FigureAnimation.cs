using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureAnimation : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private Figure figure;

    private void Awake() {
        animator = GetComponent<Animator>();
        animator.Play("IdleFigureAnimation", -1, Random.Range(0.0f, 1.0f));

    }

    public void DyingSound() {
        GetComponent<AudioSource>().Play();
    }

    public void DyingAnimation() {
        animator.SetTrigger("Dying");
    }

    public void GoHome() {
        figure.GoToHome();
    }



}
