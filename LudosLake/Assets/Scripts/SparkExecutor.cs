using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class SparkExecutor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(!GetComponent<AudioSource>().isPlaying)
            GetComponent<AudioSource>().Play();
        GetComponent<VisualEffect>().Play();
    }
}
