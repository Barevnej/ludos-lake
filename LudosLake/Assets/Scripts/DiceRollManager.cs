using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DiceRollManager : MonoBehaviour {
    [Tooltip("Helping attributes that indicate the sides of the cube.")]
    [SerializeField] private List<Transform> sides;
    
    [Space(10)]
    [Tooltip("Rigid body of the dice.")]
    [SerializeField] private Rigidbody body;
    [Tooltip("Mesh of the dice.")]
    [SerializeField] private MeshRenderer dice;

    [Space(10)]
    [Tooltip("Link on game logic.")]
    [SerializeField] private PlayerManager player;

    private Material diceMaterial;
    private Vector3 startPosition;
    private bool folling;
    private bool canRoll;

    private void Awake() {
        canRoll = true;
        startPosition = body.transform.position;
        diceMaterial = dice.material;
    }

    public void ResetToStartPosition() {
        body.transform.position = startPosition;
    }

    public void Roll() {
        if(!canRoll)
            return;

        body.AddForce(new Vector3(0, 600, 0));
        body.AddTorque(new Vector3(Random.Range(-2000, 2000), Random.Range(-2000, 2000), Random.Range(-2000, 2000)), ForceMode.Force);
        folling = false;
        canRoll = false;
    }

    private void FixedUpdate() {

        if(body.velocity.magnitude < 0.2f && !canRoll && folling) {
            GetUpSide();
            canRoll = true;
        }

        if(body.velocity.y < 0) {
            folling = true;
        }
    }

    private void Update() {
        float y = sides[0].position.y;
        int upSide = 1;
        int i = 0;

        foreach(Transform t in sides) {
            i++;
            if(y < t.position.y) {
                y = t.position.y;
                upSide = i;
            }
        }
        diceMaterial.SetFloat("_Num", upSide);
    }

    private void GetUpSide() {
        float y = sides[0].position.y;
        int upSide = 1;
        int i = 0;

        foreach(Transform t in sides) {
            i++;
            if(y < t.position.y) {
                y = t.position.y;
                upSide = i;
            }
        }
        player.SetTossedNum(upSide);
        Debug.Log("You tossed: " + upSide);
    }
}