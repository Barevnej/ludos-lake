using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;

public class Field : MonoBehaviour {
    [Header("Path Setup")]
    [Tooltip("Next field for player's figure after this.")]
    [SerializeField] private Field _nextFieldPlayer;
    [Tooltip("Next field for enemy's figure after this.")]
    [SerializeField] private Field _nextFieldEnemy;

    [Space(10)]
    [Header("Components")]
    [Tooltip("Figure that stand on this field.")]
    [SerializeField] private Figure _actualFigure;
    [Tooltip("Mesh of this field.")]
    [SerializeField] private GameObject meshObject;
    [Tooltip("Visual effect that be proct after land of figure.")]
    [SerializeField] private VisualEffect vfx;

    [Space(20)]
    [SerializeField] private GameObject fieldPref;

    [Space(30)]
    [Range(1f, 50f)]
    [SerializeField] public int numOfNextField;
    private Material material;
    private Color originalColor;
    private bool selectable;

    private void Awake() {
        material = meshObject.GetComponent<MeshRenderer>().material;
        originalColor = material.GetColor("_BaseColor");
    }

    public Field FindNextField(int tossedNum, bool enemy) {
        if(enemy && tossedNum != 0 && _nextFieldEnemy != null) {
            return _nextFieldEnemy.FindNextField(--tossedNum, enemy);
        } else if(!enemy && tossedNum != 0 && _nextFieldPlayer != null) {
            return _nextFieldPlayer.FindNextField(--tossedNum, enemy);
        } else if(tossedNum == 0) {
            if(_actualFigure != null && _actualFigure.IsEnemy().Equals(enemy))
                return null;
            return this;
        } else {
            return null;
        }
    }

    public Field GetNextField(bool enemy) {
        if(enemy) {
            return _nextFieldEnemy;
        } else {
            return _nextFieldPlayer;
        }
    }

    public void Select() {
        selectable = true;
        material.SetColor("_BaseColor", originalColor*5);
    }

    public void UnSelect() {
        selectable = false;
        material.SetColor("_BaseColor", originalColor);
    }

    public void Land() {
        selectable = false;
        vfx.Play();
        GetComponent<AudioSource>().Play();
        material.SetColor("_BaseColor", originalColor);
    }

    public void SetActualFigure(Figure figure) {
        _actualFigure = figure;
    }

    public Figure GetActualFigure() {
        return _actualFigure;
    }

    public void Setselectable(bool a) {
        selectable = a;
    }

    public bool IsSelectable() {
        return selectable;
    }

    public GameObject GetObject() {
        return meshObject;
    }

    public void SetNextField(Field next) {
        _nextFieldEnemy = next;
        _nextFieldPlayer = next;
    }

    public void CreateNextField(int max, int num) {
        if(num < max) {
            num++;

            string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(fieldPref);
            Object newObj = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(Object));
            GameObject pref = PrefabUtility.InstantiatePrefab(newObj, this.transform) as GameObject;
            pref.transform.position = new Vector3(transform.position.x + 4, transform.position.y, transform.position.z);
            pref.transform.parent = transform.parent;
            pref.name += num;
            SetNextField(pref.GetComponent<Field>());
            pref.GetComponent<Field>().CreateNextField(max, num);
        }
    }

    public void RotateOnNextField() {
        if(_nextFieldEnemy) {
            transform.LookAt(_nextFieldEnemy.transform.position);
            _nextFieldEnemy.RotateOnNextField();
        } else if(_nextFieldPlayer) {
            transform.LookAt(_nextFieldPlayer.transform.position);
            _nextFieldPlayer.RotateOnNextField();
        }
    }
}
